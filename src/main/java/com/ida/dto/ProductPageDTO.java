package com.ida.dto;

import java.util.ArrayList;
import java.util.List;

public class ProductPageDTO {
	private String description;
	private int pageNumber;
	private int productsCount;
	private int totalProductsCount;
	private List<ProductDTO> content;
	private List<String> brands;
	private List<String> subCategories;

	public ProductPageDTO() {
		content = new ArrayList<ProductDTO>();
		brands = new ArrayList<String>();
		subCategories = new ArrayList<String>();
	}

	public String getDescription() {
		return description;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public int getProductsCount() {
		return productsCount;
	}

	public int getTotalProductsCount() {
		return totalProductsCount;
	}

	public List<ProductDTO> getContent() {
		return content;
	}

	public List<String> getBrands() {
		return brands;
	}

	public List<String> getSubCategories() {
		return subCategories;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public void setproductsCount(int productsCount) {
		this.productsCount = productsCount;
	}

	public void setTotalProductsCount(int totalProductsCount) {
		this.totalProductsCount = totalProductsCount;
	}

	public void setContent(List<ProductDTO> content) {
		this.content = content;
	}
	
	public void add(ProductDTO productDTO){
		content.add(productDTO);
	}

	public void setBrands(List<String> brands) {
		this.brands = brands;
	}

	public void setSubCategories(List<String> subCategories) {
		this.subCategories = subCategories;
	}

}
