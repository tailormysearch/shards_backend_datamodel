package com.ida.dto;

import org.springframework.data.annotation.Id;

public class ProductDTO {
	@Id
	private int id;
	private String name;
	private String productType;
	private String category;
	private String subCategory;
	private String brand;
	private double price;
	private String description;
	private String imageUrl;
	
	private int rating;
	private int isNew;

	public ProductDTO() {
	}

	// Getters

	public int getId() {
		return id;
	}

	public String getname() {
		return name;
	}

	public String getCategory() {
		return category;
	}

	public String getBrand() {
		return brand;
	}

	public double getPrice() {
		return price;
	}

	public String getDescription() {
		return description;
	}

	public String getProductType() {
		return productType;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public String getImageUrl() {
		return imageUrl;
	}
	
	public int getRating() {
		return rating;
	}
	
	public int getIsNew() {
		return isNew;
	}
	
	// Setters //

	public void setId(int id) {
		this.id = id;
	}

	public void setname(String name) {
		this.name = name;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	public void setRating(int rating) {
		this.rating = rating;
	}
	
	public void setIsNew(int IsNew) {
		this.isNew = IsNew;
	}

	@Override
	public String toString() {
		return id + " [" + name + " | " + category + "]";
	}
}
